<?php

/*
 * 人人商城
 *
 * 青岛易联互动网络科技有限公司
 * http://www.we7shop.cn
 * TEL: 4000097827/18661772381/15865546761
 */
if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Set_EweiShopV2Page extends PluginWebPage {


    function main()
    {
        global $_W,$_GPC;
        $key = pdo_fetch("SELECT `key`,url FROM ".tablename('ewei_shop_open_plugin')." WHERE plugin = :plugin ", array(':plugin' => $this->pluginname));

        $key['url'] = json_decode($key['url']);
        $data = array();
        if($_W['ispost']){
            $data['url'] = $_GPC['url'];
            m('common')->updatePluginset(array('open_messikefu'=>$data),$_W['uniacid']);
           // pdo_update('ewei_shop_open_plugin',array('url'=>json_encode($_GPC['url'])),array('plugin'=>$this->pluginname));
           show_json(1);
        }
        $data = m('common')->getPluginset(array('open_messikefu'),$_W['uniacid']);
        $key['url'] = ($data['open_messikefu']['url']);
        include $this->template();
    }
}
