<?php

/*
 * 人人商城
 *
 * 青岛易联互动网络科技有限公司
 * http://www.we7shop.cn
 * TEL: 4000097827/18661772381/15865546761
 */
if (!defined('IN_IA')) {
    exit('Access Denied');
}
require_once EWEI_SHOPV2_PLUGIN . 'app/core/page_mobile.php';

class Base_EweiShopV2Page extends AppMobilePage
{

    public $wxliveModel;

    public function __construct()
    {
        parent::__construct();

        $this->wxliveModel = p('wxlive');

        if (!$this->wxliveModel) {
            die(app_error(-1, '系统为安装小程序直播'));
        }
    }

}