<?php

/*
 * 人人商城
 *
 * 青岛易联互动网络科技有限公司
 * http://www.we7shop.cn
 * TEL: 4000097827/18661772381/15865546761
 */
if (!defined('IN_IA')) {
	exit('Access Denied');
}

class Show_EweiShopV2Page extends PluginWebPage
{

    function main()
    {
        global $_W, $_GPC;
        $id =  intval($_GPC['id']);
        //商品海报的商品ID 非商品海报该ID为空
        $goodsId = $_GPC['goodsid'];
        $poster = pdo_fetch('select * from ' . tablename('ewei_shop_postera') . ' where id=:id and uniacid=:uniacid limit 1', array(':id' => $id, ':uniacid' => $_W['uniacid']));
        // 调用 model 自定义方法
        $this->model->create_folder();
        $httpImg = $this->model->previewPoster($poster,$goodsId,$id);
        header("Location: $httpImg", true, 301);
    }


}
