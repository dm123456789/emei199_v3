<?php

/*
 * 人人商城
 *
 * 青岛易联互动网络科技有限公司
 * http://www.we7shop.cn
 * TEL: 4000097827/18661772381/15865546761
 */
if (!defined('IN_IA')) {
    exit('Access Denied');
}

class Index_EweiShopV2Page extends PluginMobilePage
{

    public function main()
    {
        global $_W, $_GPC;

        $activity_id = isset($_GPC['id']) ? $_GPC['id'] : null;
        $activity = $this->model->getActivity($activity_id);
        // 活动不存在跳转到首页
        if (!$activity) {
            header('location:' . mobileUrl('', '', true));
        }
        $user = m('member')->getMember($_W['openid']);
        $mid = isset($_GPC['mid']) ? $_GPC['mid'] : null;
        $isShare = false;
        if (isset($mid) && $mid != $user['id']) {
            $isShare = true;
        }
        // 是否领取过任务
        $isReceive = false;
        // 活动是否已经成功
        $success = false;
        $activityData = array();
        $shareParams = array('id' => $activity['id']);
        $overTime = 0;
        if (!$user) {
            header('location:' . mobileUrl('account/login', '', true));
        }
        // 验证活动时效性,如果有问题存储错误信息
        $activitySetting = $activity;
        if (!$this->model->validateActivity($_GPC['id'])) {
            if ($this->model->errno) {
                if ($isShare) {
                    $takePartInUsers = pdo_fetchall("select openid from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = {$_W['uniacid']} and headerid = {$mid}");
                } else {
                    $takePartInUsers = pdo_fetchall("select openid from" . tablename('ewei_shop_friendcoupon_data') . " where uniacid = {$_W['uniacid']} and activity_id = {$activity_id}");
                }
                $openIds = array_column($takePartInUsers, 'openid');
                in_array($user['openid'], $openIds) && $tips = '';
            } else {
                $tips = $this->model->errmsg;
            }
//            $tips = $this->model->errmsg;
        }


        // 活动说明按照回车切成数组
        $activitySetting['desc'] = explode("\r\n", $activitySetting['desc']);
        // 剩余人数默认是活动设置总人数
        $overPeople = $activitySetting['people_count'];
        // 邀请人数  假如五人团，因为自己领取的时候就已经瓜分过一次了，要显示`赶快邀请4个好友共同瓜分`,
        $numberOfInvitations = $activitySetting['people_count'] - 1;
        // 是分享链接的话,处理
        if ($isShare) {
            $share_user = m('member')->getMember($mid);
            // 分享人活动信息
            $shareActivityInfo = $this->model->getCurrentActivityInfo($share_user['openid'], $activity['id']);
            $currentActivityInfo = $this->model->getCurrentActivityInfo($user['openid'], $activity['id']);
            $overTime = $shareActivityInfo['deadline'];
            // 分享人活动不存在的时候
            if (!$shareActivityInfo) {
                $tips == '' && $tips = '分享的活动已经不存在了哦';
            }

            if (!empty($currentActivityInfo) && $currentActivityInfo['headerid'] != $shareActivityInfo['headerid']) {
                $tips == '' && $tips = '您已经参与过这个活动啦，去看看别的活动吧~';
                $_GPC['mid'] = null;
                $mylink = mobileUrl('friendcoupon', array('id' => $currentActivityInfo['activity_id']));
            }
            if ($currentActivityInfo) {
                $overTime = $currentActivityInfo['deadline'];
                $isReceive = true;
                $activities = $this->model->getOngoingActivities($shareActivityInfo['activity_id'], $shareActivityInfo['headerid']);
                foreach ($activities as $activity) {
                    // 没有openid,则是当前没有参加的人
                    if (!$activity['openid']) {
                        $overPeople++;
                    }
                    // openid不是空,放入已经参与的用户中去
                    if ($activity['openid'] != '') {
                        $activityData[] = $activity;
                    }
                }
            }


            if ($currentActivityInfo['status'] == 1) {
                $success = true;

            } else { // 当前活动没有成功
                $activityData = array();
                // 当前还缺几个人瓜分成功
                $overPeople = 0;
                // 获取所有进行中的活动
                $activities = $this->model->getOngoingActivities($shareActivityInfo['activity_id'], $shareActivityInfo['headerid']);
                foreach ($activities as $activity) {
                    // 没有openid,则是当前没有参加的人
                    if (!$activity['openid']) {
                        $overPeople++;
                    }
                    // openid不是空,放入已经参与的用户中去
                    if ($activity['openid'] != '') {
                        $activityData[] = $activity;
                    }
                }
            }


        } else {
            // 不是分享链接
            $share_user = $user;
            // 获取当前用户的任务
            $currentActivityInfo = $this->model->getCurrentActivityInfo($user['openid'], $activity['id']);
            // 如果存在当前任务,认为用户领取了任务
            if ($currentActivityInfo) {
                $isReceive = true;
            }
            // 任务成功之后会把所有的status更新成1,所以当前任务成功认为所有任务都成功了
            if ($currentActivityInfo['status'] == 1) {
                $success = true;
                $activities = $this->model->getOngoingActivities($currentActivityInfo['activity_id'], $currentActivityInfo['headerid']);
                foreach ($activities as $activity) {
                    // 没有openid,则是当前没有参加的人
                    if (!$activity['openid']) {
                        $overPeople++;
                    }
                    // openid不是空,放入已经参与的用户中去
                    if ($activity['openid'] != '') {
                        $activityData[] = $activity;
                    }
                }
            } else { // 当前活动没有成功
                // 如果没有分享人,默认认为自己是分享人
                $overTime = $currentActivityInfo['deadline'];
                $share_user = $user;
                // 当前还缺几个人瓜分成功
                $overPeople = 0;
                // 获取所有进行中的活动
                $activities = $this->model->getOngoingActivities($currentActivityInfo['activity_id'], $currentActivityInfo['headerid']);
                foreach ($activities as $activity) {
                    // 没有openid,则是当前没有参加的人
                    if (!$activity['openid']) {
                        $overPeople++;
                    }
                    // openid不是空,放入已经参与的用户中去
                    if ($activity['openid'] != '') {
                        $activityData[] = $activity;
                    }
                }
            }
        }


        $shareParams['mid'] = $share_user['id'];
        $activity['desc'] = explode("\r\n", $activity['desc']);


        if ($success) {
            $coupon = pdo_fetch("select id,couponid,openid,uniacid,used from " . tablename('ewei_shop_coupon_data') . " where friendcouponid = :friendcouponid", array('friendcouponid' => $currentActivityInfo['id']));
        }


        // 当前活动已经完成
        if ($currentActivityInfo['status'] == 1 || $shareActivityInfo['status'] == 1) {
            $takePartInUserIds = array_column($activityData, 'openid');
            if (!in_array($user['openid'], $takePartInUserIds)) {
                $tips == '' && $tips = '当前活动已经瓜分成功,下回要快点哦~';
                $_GPC['mid'] = null;
            }
        }

        // 当前活动未完成超时的情况下
        if (isset($overTime) && (int)time() > $overTime && !$success) {
            $tips == '' && $tips = '很遗憾，没有在规定时间内完成瓜分，下次要快一点哦~';
        }

        if (isset($overTime) && (int)time() > (int)$overTime && !$success && $isReceive) {
            $tips == '' && $tips = '很遗憾，没有在规定时间内完成瓜分，下次要快一点哦~';
        }
        $_W['shopshare'] = array(
            'title' => $activitySetting['title'],
            'imgUrl' => $_W['siteroot'] . "addons/ewei_shopv2/static/images/friendcoupon.png",
            'desc' => "{$share_user['nickname']}邀请您一起瓜分{$activitySetting['coupon_money']}元",
            'link' => mobileUrl('friendcoupon', $shareParams, true)
        );

        // 清除分享人id
        if ($tips != '') {
            $_GPC['mid'] = null;
        }

        $activityDataLen = count($activityData);
        $activityData = array_slice($activityData, 0, 5);
        $backFullUrl = mobileUrl('', '', true);

        // 这个地方处理是因为点击返回商城的时候自动携带了mid参数,要给他卸载掉,重新拼成链接
        $back_url = parse_url($backFullUrl);
        $r = explode('&', $back_url['query']);
        foreach ($r as $key => $item) {
            if (strpos($item, 'mid=') !== false) {
                unset($r[$key]);
            }
        }
        $queryString = implode($r, '&');
        $back = $back_url['scheme'] . "://" . $back_url['host'] . $back_url['path'] . "?" . $queryString;
        include $this->template();
    }

    /**
     * 领取任务,每个任务每个人只允许参加一次
     */
    public function receive()
    {
        global $_W, $_GPC;
        // 当前用户
        $user = m('member')->getMember($_W['openid']);
        // 活动id
        $activity_id = (int)$_GPC['id'];
        if (!$activity = $this->model->validateActivity($activity_id)) {
            show_json(0, $this->model->errmsg);
        }


        $currentActivityInfo = $this->model->getCurrentActivityInfo($user['openid'], $activity_id);
        $isTakePartIn = (boolean)$currentActivityInfo;
        // 如果用户参加过活动了,
        $isTakePartIn && show_json(0, '您已经参加了当前活动,请不要重复领取！');
        // 计算出来的优惠券金额
        $couponAmounts = array();
        // 0-随机优惠券, 1-平均优惠券
        switch ($activity['allocate']) {
            case 0:
                // 红包最大值不超过每人可以获取的
                $totalMoney = $activity['coupon_money']; // 发放的总金额
                $peopleCount = (int)$activity['people_count']; // 发放的总人数(此处必须为整型,否则会无输出)
                $minimum = $activity['upper_limit']; // 最小金额
                $couponAmounts = $this->model->randomCouponAlgorithm($totalMoney, $peopleCount, $minimum);
                break;
            case 1:
                $couponAmounts = $this->model->avgCouponAlgorithm($activity['coupon_money'], $activity['people_count']);
                break;
        }

        // 获得所有进行中的活动
        $couponActivities = $this->model->getOngoingActivities($currentActivityInfo['id'], $currentActivityInfo['headerid']);
        // 下发所有瓜分券,空出openid,后面刮一个给塞一个数据
        $currentActivityIds = array();
        $time = time();
        // 创建优惠券的时候就把优惠券活动时间计算出来
        if (!$couponActivities) {
            // 活动结束时间
            $deadline = $time + $activity['duration'] * 3600 < $activity['activity_end_time'] ?
                $time + $activity['duration'] * 3600 :
                $activity['activity_end_time'];

            foreach ($couponAmounts as $couponAmount) {
                $data = array(
                    'uniacid' => $_W['uniacid'],
                    'activity_id' => $activity['id'],
                    'headerid' => $user['id'],
                    'deduct' => (float)$couponAmount,
                    'enough' => $activity['use_condition'], //满多少可用,不填写相当于空，对应
                    'deadline' => $deadline
                );
                // 生成所有随机优惠券
                pdo_insert('ewei_shop_friendcoupon_data', $data);
                $currentActivityIds[] = pdo_insertid();
            }
        } else {
            $currentActivityIds[] = array_column($couponActivities, 'id');
        }

        // 找出最小的id,
        $headerid = min($currentActivityIds);
        // 把优惠券先发给队长
        pdo_update('ewei_shop_friendcoupon_data', array(
            'openid' => $user['openid'],
            'avatar' => $user['avatar'],
            'nickname' => $user['nickname'],
            'receive_time' => time(), // 领取活动的时间
        ), array('id' => $headerid));

        pdo_update('ewei_shop_friendcoupon', array('launches_count +=' => 1), array('id' => $activity['id']));


        // 发送模板消息
        $commonModel = m('common');
        $noticeModel = m('notice');
        // 消息通知发送
        $tm = $commonModel->getSysset('notice');
        /* 获取当前活动信息 */
        $currentActivityInfo = $this->model->getCurrentActivityInfo($user['openid'], $activity_id);
        // 获取优惠券名称
        if ($currentActivityInfo['enough'] > 0) {
            $couponName = "满{$currentActivityInfo['enough']}减{$currentActivityInfo['deduct']}元优惠券";
        } else {
            $couponName = "无门槛减{$currentActivityInfo['deduct']}元优惠券";
        }
        // 根据是否开启高级模式发送模板消息
        if ($tm['friendcoupon_launch_close_advanced']) {
            $noticeModel->sendNotice(array(
                'openid' => $user['openid'],
                'tag' => 'friendcoupon_launch',
                'datas' => array(
                    array('name' => '活动名称', 'value' => $activity['title']),
                    array('name' => '活动开始时间', 'value' => $this->model->dateFormat($currentActivityInfo['receive_time'])),
                    array('name' => '活动结束时间', 'value' => $this->model->dateFormat($currentActivityInfo['deadline'])),
                    array('name' => '瓜分券领取时间', 'value' => $this->model->dateFormat(time())),
                    array('name' => '瓜分券名称', 'value' => $couponName))
            ));
        } else {
            // 这是默认模板消息
            $params = array(
                'activity_title' => $activity['title'],
                'activity_start_time' => $this->model->dateFormat($currentActivityInfo['receive_time']),
                'activity_end_time' => $this->model->dateFormat($currentActivityInfo['deadline']),
                'receiveTime' => $this->model->dateFormat(time()),
                'couponName' => $couponName,
                'url' => mobileUrl('friendcoupon', array('id' => $currentActivityInfo['activity_id'], 'mid' => $currentActivityInfo['headerid']), true)
            );
            m('notice')->sendFriendCouponTemplateMessage($user['openid'], $params, 'launch');
        }

        // TODO 是否加一个活动log表
        show_json(1, '活动领取成功');
    }

    /**
     * 立即瓜分接口
     */
    public function divide()
    {
        global $_W, $_GPC;
        $activity_id = $_GPC['id'];
        $mid = $_GPC['mid']; // 活动发起者Id
        $share_user = m('member')->getMember($mid);
        $user = m('member')->getMember($_W['openid']);// 当前登录的用户
        $activity = $this->model->getActivity($activity_id);

        // 查看当前活动是否还在
        $onGoingActivities = $this->model->getOngoingActivities($activity_id, $mid);

        if (!$onGoingActivities) {
            show_json(0, array(
                'url' => mobileUrl('', '', true),
                'message' => '没有进行中的活动'
            ));
        }

        // 获取当前分享的活动的信息
        $shareActivityInfo = $this->model->getCurrentActivityInfo($share_user['openid'], $activity['id']);

        if ($shareActivityInfo['status'] == 1) {
            show_json(0, '活动已经完成<br>下次早点来啊~');
        }

        // 看下当前用户是否参与了活动
        $currentActivityInfo = $this->model->getCurrentActivityInfo($user['openid'], $activity_id);
        if (!$currentActivityInfo) {
            // 获取所有的优惠券
            $coupons = pdo_fetchall("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and activity_id = :activity_id and headerid = :headerid", array(
                ':uniacid' => $_W['uniacid'],
                ':activity_id' => $activity['id'],
                ':headerid' => $mid,
            ));
            foreach ($coupons as $coupon) {
                // 没有openid的就是没有下发的优惠券,找到对应记录,更新,然后跳转
                if (!$coupon['openid']) {
                    pdo_update('ewei_shop_friendcoupon_data', array(
                        'openid' => $user['openid'],
                        'status' => 0,
                        'avatar' => $user['avatar'],
                        'nickname' => $user['nickname'],
                        'receive_time' => time(), // 领取活动的时间
                    ), array('id' => $coupon['id']));
                    break;
                }
            }

            // 获取下剩余优惠券数量
            $overPlus = pdo_fetchcolumn("select count(1) from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and headerid = :headerid and activity_id = :activity_id and openid = ''", array(
                ':uniacid' => $_W['uniacid'],
                ':headerid' => $mid,
                ':activity_id' => $activity_id,
            ));

            // 如果还没有完全发完,就进入分享页面等待
            if ($overPlus) {
                show_json(1, '瓜分成功!');
            }
            pdo_update('ewei_shop_friendcoupon_data', array('status' => 1), array('activity_id' => $activity_id, 'headerid' => $mid));
            // 整个活动就成功了
            //重新获取下优惠券
            $sql = "select f.*, fd.enough, fd.id as friendcouponid,fd.deduct,fd.openid,fd.activity_id,fd.receive_time,fd.deadline from" . tablename('ewei_shop_friendcoupon_data') . " fd "
                . " left join " . tablename('ewei_shop_friendcoupon') . " f on fd.activity_id = f.id"
                . " where fd.uniacid = :uniacid and fd.headerid = :headerid and fd.activity_id = :activity_id";

            $coupons = pdo_fetchall($sql, array(
                ':uniacid' => $_W['uniacid'],
                ':activity_id' => $activity['id'],
                ':headerid' => $mid
            ));


            // 发送优惠券
            if (false === $this->model->sendFriendCoupon($coupons)) {
                show_json(0, $this->model->errmsg);
            }

            $commonModel = m('common');
            $noticeModel = m('notice');
            $tm = $commonModel->getSysset('notice');
            //发送消息通知
            foreach ($coupons as $coupon) {
                $url = mobileUrl('friendcoupon', array('id' => $coupon['activity_id'], 'mid' => $coupon['headerid']), true);
                if ($coupon['enough'] > 0) {
                    $couponName = "满{$coupon['enough']}减{$coupon['deduct']}元优惠券";
                } else {
                    $couponName = "无门槛减{$coupon['deduct']}元优惠券";
                }
                // 根据是否开启高级模式发送模板消息
                if ($tm['friendcoupon_launch_close_advanced']) {
                    $noticeModel->sendNotice(array(
                        'openid' => $coupon['openid'],
                        'tag' => 'friendcoupon_complete',
                        'datas' => array(
                            array('name' => '活动名称', 'value' => $activity['title']),
                            array('name' => '活动开始时间', 'value' => $this->model->dateFormat($coupon['receive_time'])),
                            array('name' => '活动结束时间', 'value' => $this->model->dateFormat($coupon['deadline'])),
                            array('name' => '瓜分券领取时间', 'value' => $this->model->dateFormat(time())),
                            array('name' => '瓜分券名称', 'value' => $couponName)
                        ),
                        'url' => $url,
                    ));
                } else {
                    // 这是默认模板消息
                    $params = array(
                        'activity_title' => $activity['title'],
                        'activity_start_time' => $this->model->dateFormat($currentActivityInfo['receive_time']),
                        'activity_end_time' => $this->model->dateFormat($currentActivityInfo['deadline']),
                        'receiveTime' => $this->model->dateFormat(time()),
                        'couponName' => $couponName,
                        'url' => $url
                    );
                    m('notice')->sendFriendCouponTemplateMessage($coupon['openid'], $params, 'complete');
                }

            }
            // 下发所有优惠券
            show_json(1, "活动参与成功!");
        }
        show_json(0, '您已经瓜分过优惠券了哦!');
    }

    // 获取已经参加活动的人的信息
    public function getTakePartInPeopleData($activity_id, $headerid)
    {
        global $_W, $_GPC;
        $activity_id = $_GPC['activity_id'];
        $headerid = $_GPC['mid'];

        $r = pdo_fetchall("select * from " . tablename('ewei_shop_friendcoupon_data') . " where unaicid = :unaicid and headerid = :headerid and activity_id = :activity_id", array(
            ':uniacid' => $_W['uniacid'],
            ':headerid' => $headerid,
            ':activity_id' => $activity_id
        ));
    }

    // 查看更多好友
    public function more()
    {
        global $_W, $_GPC;

        $activity_id = $_GPC['id']; // 活动id
        $mid = $_GPC['mid']; // 分享人id
        // 通过上面两个字段获取一个确定的活动
        $pindex = max(1, $_GPC['pindex']);
        $psize = 10;

        // 当前正在参与的活动
        if (empty($mid)) {
            $currentTakePartActivity = pdo_fetch("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and openid = :openid and activity_id = :activity_id", array(
                ':uniacid'  =>  $_W['uniacid'],
                ':openid'   => $_W['openid'],
                ':activity_id'  => $activity_id,
            ));
            $mid = $currentTakePartActivity['headerid'];
        }

        $list = pdo_fetchall("select avatar,nickname,deduct from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and activity_id = :activity_id and headerid = :headerid and openid <> '' limit " . ($pindex - 1) . "," . $psize, array(
            ':uniacid' => $_W['uniacid'],
            ':headerid' => $mid,
            ':activity_id' => $activity_id
        ));


        show_json(0, array(
            'list' => $list
        ));
    }

    // 倒计时
    public function deadline()
    {
        global $_W, $_GPC;
        $id = $_GPC['id']; // 活动Id
        $mid = isset($_GPC['mid']) ? $_GPC['mid'] : null;
        $currentActivityInfo = $this->model->getCurrentActivityInfo($_W['openid'], $id);
        if (!$currentActivityInfo) {
            $deadline = $this->model->getOngoingActivities($id, $mid)[0]['deadline'];
        } else {
            $deadline = $currentActivityInfo['deadline'];
        }
        show_json(0, array(
            'deadline' => $deadline
        ));
    }

}
