<?php
/*
 * 人人商城
 *
 * 青岛易联互动网络科技有限公司
 * http://www.we7shop.cn
 * TEL: 4000097827/18661772381/15865546761
 */

if (!defined('IN_IA')) {

    exit('Access Denied');

}

class Notify_EweiShopV2Page extends PluginWebPage
{
    function main()
    {
        global $_W, $_GPC;
        $commonModel = m('common');
        $sets = m('common')->getSysset(array('app', 'pay'));
        $tmsg_list = pdo_fetchall("SELECT id, `name` FROM " . tablename('ewei_shop_wxapp_tmessage') . 'WHERE uniacid=:uniacid AND status=1', array(':uniacid' => $_W['uniacid']));
        $template_list = pdo_fetchall('SELECT id,title,typecode FROM ' . tablename('ewei_shop_member_message_template') . ' WHERE uniacid=:uniacid ', array(':uniacid' => $_W['uniacid']));
        $templatetype_list = pdo_fetchall('SELECT * FROM  ' . tablename('ewei_shop_member_message_template_type'));

        $template_group = array();
        foreach ($templatetype_list as $type) {
            $templates = array();

            foreach ($template_list as $template) {
                if ($template['typecode'] == $type['typecode']) {
                    $templates[] = $template;
                }
            }
            $template_group[$type['typecode']] = $templates;
        }
        $data = $commonModel->getSysset('notice');

        if ($_W['ispost']) {
            // 获取前台传过来的数值
            $notify = isset($_GPC['notify']) ? $_GPC['notify'] : null;
            $data = $commonModel->getSysset('notice');
            $notice = array_merge($data, $notify);
            $commonModel->updateSysset(array('notice' => $notice));
//            $r = $commonModel->getSysset('notice');
            show_json(1);
        }

        include $this->template();
    }

    public function setTemplate()
    {
        global $_W, $_GPC;

        $tag = isset($_GPC['tag']) ? trim($_GPC['tag']) : null;
        $isOpen = isset($_GPC['checked']) ? (boolean)$_GPC['checked'] : null;

        $allowTags = array('wxapp_complete');
        if (!in_array($tag, $allowTags)) {
            show_json(0, '非法模板类型');
        }
        // 小程序的默认模板类型
        if (strpos($tag, 'wxapp_') !== false) {
            $token = $this->model->getToken(true);
            $typecode = str_replace('wxapp_', 'friendcoupon_', $tag);
            // ewei_shop_member_wxapp_message_template_type 小程序预设模板表
            // ewei_shop_member_wxapp_message_template_default 每个公众号默认的模板id

            // 获取当前公众号看下是否存在默认模板Id
            $templateId = pdo_fetchcolumn("select templateid from" . tablename('ewei_shop_member_wxapp_message_template_default') . " where uniacid = :uniacid and typecode = :typecode", array(
                ':uniacid' => $_W['uniacid'],
                ':typecode' => $typecode
            ));

            // 小程序每次最多能获取20条模板数据,但是后台最多设置25个数据,要请求两次,拼接一下数据
            $request_first = ihttp_post('https://api.weixin.qq.com/cgi-bin/wxopen/template/list?access_token='.$token, json_encode(array('offset' => 0, 'count' => 20)));
            $request_second = ihttp_post('https://api.weixin.qq.com/cgi-bin/wxopen/template/list?access_token='.$token, json_encode(array('offset' => 20, 'count' => 6)));

            $templateList1 = json_decode($request_first['content'], true);
            $templateList2 = json_decode($request_second['content'], true);

            // 合并后是最后的列表
            $templateList = array_merge($templateList1['list'], $templateList2['list']);
            $templateIds = array_column($templateList, 'template_id');

            $templatenum = count($templateIds);

            // 如果默认模板id不存在,或者当前数据库的模板不在模板列表数组中,需要重新生成模板
            if (!$templateId || !in_array($templateId, $templateIds)) {
                $defaultTemplate = pdo_fetch("select * from " . tablename('ewei_shop_member_wxapp_message_template_type') . " where typecode = :typecode", array(':typecode' => $typecode));
                if($templatenum>=25)
                {
                    show_json(1,array('status'=>0,'messages'=>'开启'.$defaultTemplate['name'].'失败！！您的可用微信模板消息数量达到上限，请删除部分后重试！！','tag'=>$tag));
                }

                // 获取数据库中写死的默认模板
                if (!$defaultTemplate) {
                    show_json(0, '默认模板不存在');
                }

                $_postData = array(
                    'id' => $defaultTemplate['templatecode'],
                    'keyword_id_list' => explode(',', $defaultTemplate['keyword_id_list'])
                );
                
                // 通过接口把消息添加到模板后台
                $ret = ihttp_post('https://api.weixin.qq.com/cgi-bin/wxopen/template/add?access_token=' . $token, json_encode($_postData));
                $ret = json_decode($ret['content'], true);
                if ($ret['errcode'] == 45026) {
                    show_json(0, array(
                        'status' => 0,
                        'messages' => '模板超过了25个，请到小程序后台删除部分模板后重试'));
                }

                if ($ret['errcode'] == 0) {

                    // 如果默认模板不存在,那么写入,否则更新
                    if (!$templateId) {
                        pdo_insert('ewei_shop_member_wxapp_message_template_default', array(
                            'typecode' => $typecode,
                            'uniacid' => $_W['uniacid'],
                            'templateid' => $ret['template_id'],
                            'datas' => 'a:3:{i:0;a:2:{s:3:"key";s:17:"{{keyword1.DATA}}";s:5:"value";s:27:"瓜分券活动完成通知";}i:1;a:2:{s:3:"key";s:17:"{{keyword2.DATA}}";s:5:"value";s:26:"[活动名称]活动完成";}i:2;a:2:{s:3:"key";s:17:"{{keyword3.DATA}}";s:5:"value";s:52:"您于[瓜分券领取时间]领取[瓜分券名称]";}}'
                        ));
                    } else {
                        pdo_update('ewei_shop_member_wxapp_message_template_default', array(
                            'templateid'    => $ret['template_id'],
                        ), array(
                            'uniacid'   => $_W['uniacid'],
                            'typecode'  => $typecode
                        ));
                    }
                }
            }
            show_json(1, array(
                'status' => 1,
                'tag'   => $tag
            ));
        }
    }


}