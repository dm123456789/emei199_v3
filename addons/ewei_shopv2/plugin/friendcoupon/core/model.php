<?php
/*
 * 人人商城
 *
 * 青岛易联互动网络科技有限公司
 * http://www.we7shop.cn
 * TEL: 4000097827/18661772381/15865546761
 */

if (!defined('IN_IA')) {

    exit('Access Denied');

}

class FriendcouponModel extends PluginModel
{
    public $errmsg = null;
    public $errno = 0;

    const WXAPP_PREFIX = 'wxapp_';
    const SWITCH_SUFFIX = '_switch';


    /**
     * friendcoupon表里面
     * status -1活动失效（过期，结束） 0 活动未开始 1 活动进行中
     * stop_time 存在 && status == -1 的时候，管理员手动关闭了活动
     */

    /**
     * 获取用户的瓜分券
     * @param $friendcouponid
     * @param $openid
     * @return bool
     */
    public function getFriendCoupon($openid, $friendcouponid)
    {
        global $_W;
        return pdo_fetch("select * from " . tablename('ewei_shop_coupon_data') . " where uniacid = :uniacid and openid = :openid and friendcouponid = :friendcoupoinid", array(
            ':uniacid'        => $_W['uniacid'],
            ':openid'         => $openid,
            ':friendcouponid' => $friendcouponid
        ));
    }

    /**
     * 获取活动列表
     */
    public function getActivityList()
    {
        global $_W;
        return pdo_fetchall("select * from " . tablename('ewei_shop_friendcoupon') . " where uniacid = :uniacid and deleted = :deleted order by create_time desc", array(
            ":uniacid" => $_W['uniacid'],
            ":deleted" => 0
        ));
    }

    // 平均优惠券算法, 向下取整
    public function avgCouponAlgorithm($coupon_money, $people_count)
    {
        $avg_amount = floor($coupon_money / $people_count * 100) / 100;
        $previous = array_fill(0, $people_count - 1, $avg_amount);
        $last_one = $coupon_money - ($avg_amount * ($people_count - 1));
        $ret = array_merge(array($last_one), $previous);
        shuffle($ret);
        return $ret;
    }


    /**
     * @param $total
     * @param $num
     * @param float $min
     * @return array
     */
    public function randomCouponAlgorithm($total, $num, $min = 0.01)
    {
        $overPlus = $total - $num * $min; // 剩余待发钱数
        $base = 0; // 总基数
        // 存放所有人数据
        $container = array();
        // 每个人保底
        for ($i = 0; $i < $num; $i++) {
            // 计算比重
            $weight = round(lcg_value() * 1000);
            $container[$i]['weight'] = $weight; // 权重
            $container[$i]['money'] = $min; // 最小值都塞进去
            $base += $weight; // 总基数
        }

        $len = $num - 1; // 下面要计算总人数-1的数据,
        for ($i = 0; $i < $len; $i++) {
            $money = floor($container[$i]['weight'] / $base * $overPlus * 100) / 100; // 向下取整,否则会超出
            $container[$i]['money'] += $money;
        }

        // 弹出最后一个元素
        array_pop($container);
        $result = array_column($container, 'money');
        $last_one = round($total - array_sum($result), 2);
        array_push($result, $last_one);
        return $result;
    }


    /**
     * 检查用户是否参加过这个活动
     * @param $openid string 用户Openid
     * @param $activity_id int 活动id
     * @return bool|false 返回参加过的活动|false
     */
    public function isJoinActivity($openid, $activity_id)
    {
        global $_W;
        return pdo_fetch("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and openid = :openid and activity_id = :activity_id", array(
            ':openid'      => $openid,
            ':uniacid'     => $_W['uniacid'],
            ':activity_id' => $activity_id
        ));
    }


    /**
     * 获取当前用户参加的活动的信息
     * @param $openid
     * @param $activity_id
     * @return bool
     */
    public function getCurrentActivityInfo($openid, $activity_id)
    {
        global $_W;
        return pdo_fetch("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and openid = :openid and activity_id = :activity_id", array(
            ':openid'      => $openid,
            ':uniacid'     => $_W['uniacid'],
            ':activity_id' => $activity_id
        ));
    }

    /**
     * 获取当前正在进行的活动
     * @param $activity_id
     * @param $headerid
     * @return array|false 返回正在进行的活动,如果没有返回false
     */
    public function getOngoingActivities($activity_id, $headerid)
    {
        global $_W;

        return pdo_fetchall("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and activity_id = :activity_id and headerid = :headerid", array(
            ":headerid"    => $headerid,
            ':activity_id' => $activity_id,
            ':uniacid'     => $_W['uniacid']
        ));


    }

    /**
     * 获取活动,不传入id返回所有活动
     * @param int $id
     * @return array|bool
     */
    public function getActivity($id = null)
    {
        global $_W;
        $table = tablename('ewei_shop_friendcoupon');
        if (is_null($id)) {
            return pdo_fetchall("select * from {$table} where uniacid = :uniacid", array(':uniacid' => $_W['uniacid']));
        }
        return pdo_fetch("select * from {$table} where id  = :id", array(':id' => $id));
    }


    /**
     * 验证活动有效性,验证成功返回活动,验证失败返回false
     * @param $verifyItem array|int
     * @return bool|array
     */
    public function validateActivity($id)
    {

        // 获取活动
        $activity = $this->getActivity($id);

        // 获取当前时间
        $time = time();
        // 活动不存在
        if (!$activity) {
            $this->errmsg = '活动不存在';
            return false;
        }
        // 当前时间小于活动开始时间
        if ($time < $activity['activity_start_time']) {

            $this->errmsg = "活动未开始！";
            return false;
        }

        if (($time > $activity['activity_end_time']) || (!empty($activity['stop_time']) && $time > $activity['stop_time'])) {
            $this->errno = -10001;
            $this->errmsg = '活动已经结束！<br>下次早点来啊~!';
            return false;
        }

        // 活动可发起次数验证
        if ((int)$activity['launches_limit'] === (int)$activity['launches_count']) {
            $this->errno = -10002;
            $this->errmsg = '活动已经没有可发起次数!';
            return false;
        }

        if ((int)$activity['status'] === -1) {
            $this->errmsg = '当前活动已经失效！';
            return false;
        }

        return $activity;
    }


    /**
     * 下发好友瓜分券
     * @param $coupons array,已经成功的瓜分券接口
     * @return bool
     */
    public function sendFriendCoupon($coupons)
    {
        global $_W;

        try {
            foreach ($coupons as $coupon) {
                $data = array(
                    'uniacid'           => $_W['uniacid'],
                    'couponname'        => '瓜分券',
                    'enough'            => $coupon['enough'],
                    'deduct'            => $coupon['deduct'],
                    'timelimit'         => $coupon['use_time_limit'],
                    'timestart'         => $coupon['use_start_time'],
                    'timeend'           => $coupon['use_end_time'],
                    'timedays'          => $coupon['use_valid_days'],
                    'createtime'        => time(),
                    'total'             => 1,
                    'coupontype'        => 0,
                    'limitdiscounttype' => $coupon['limitdiscounttype'],
                    'limitgoodcatetype' => $coupon['limitgoodcatetype'],
                    'limitgoodcateids'  => $coupon['limitgoodcateids'],
                    'limitgoodtype'     => $coupon['limitgoodtype'],
                    'limitgoodids'      => $coupon['limitgoodids'],
                    'isfriendcoupon'    => 1
                );
                // 写入优惠券表
                pdo_insert('ewei_shop_coupon', $data);
                $insert_id = pdo_insertid();

                $data['openid'] = $coupon['openid'];
                $data['friendcouponid'] = $coupon['friendcouponid'];
                $couponData[$insert_id] = $data;
            }

            foreach ($couponData as $id => $coupon) {
                $data = array(
                    'couponid'       => $id,
                    'uniacid'        => $_W['uniacid'],
                    'gettype'        => 0,
                    'openid'         => $coupon['openid'],
                    'gettime'        => time(),
                    'friendcouponid' => $coupon['friendcouponid']
                );
                pdo_insert('ewei_shop_coupon_data', $data);
            }
        } catch (Exception $e) {
            $this->errmsg = '优惠券下发失败';
            return false;
        }
        return true;
    }

    /**
     * 验证数组是否为多维数组
     * @param $array
     * @return bool
     */
    protected function isMultiArray($array)
    {
        if (!is_array($array)) {
            return false;
        }
        return count($array) === count($array, 1);
    }

    // 返回格式化时间
    public function dateFormat($timestamp, $format = 'Y-m-d H:i:s')
    {
        return date($format, $timestamp);
    }

    // 获取参加过活动的人的数据
    public function getTakePartUserData($activity_id, $headerid)
    {
        global $_W;
        return pdo_fetchall("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and headerid = :headerid and activity_id = :activity_id and openid <> ''", array(
            ':uniacid'     => $_W['uniacid'],
            ':headerid'    => $headerid,
            ':activity_id' => $activity_id
        ));
    }

    // 检查活动是否已经结束
    public function isSuccess($activity_id, $headerid)
    {
        // 已经参加的用户的总数
        return count($this->getTakePartUserData($activity_id, $headerid)) == $this->getActivity($activity_id)['people_count'];
    }


    /**
     * 根据openid获取用户
     * @param $openid
     * @return bool|array
     */
    public function getMember($openid)
    {
        global $_W;
        return pdo_fetch("select * from " . tablename('ewei_shop_member') . " where uniacid = :uniacid and openid = :openid", array(
            ':uniacid' => $_W['uniacid'],
            ':openid'  => $openid
        ));
    }
    //获取微信的openid方法
    public function getmemberwx($openid_wx)
    {

        global $_W;
        return pdo_fetch("select * from " . tablename('ewei_shop_member') . " where uniacid = :uniacid and openid_wa = :openid_wa", array(
            ':uniacid' => $_W['uniacid'],
            ':openid_wa'  => $openid_wx
        ));
    }


    // 获取参加活动的人的信息
    public function getTakePartInPeopleData($activity_id, $headerid)
    {
        global $_W;
        $r = pdo_fetchall("select * from " . tablename('ewei_shop_friendcoupon_data') . " where uniacid = :uniacid and headerid = :headerid and activity_id = :activity_id", array(
            ':uniacid'     => $_W['uniacid'],
            ':headerid'    => $headerid,
            ':activity_id' => $activity_id
        ));
    }

    /**
     * 引入随机优惠券算法
     */
    public function import_redPackBuilder()
    {
        $path = IA_ROOT . "/addons/ewei_shopv2/plugin/friendcoupon/core/redPackBuilder.php";
        require_once $path;
    }

    /**
     * 获取微信的模板消息,公众号的可以通过m('notice')->sendNotice()方法解决
     * @param $type
     * @return array|bool
     */
    public function getTemplateMessage($type)
    {
        global $_W;

        $allowTypes = array('complete');
        // 检测类型
        if (!in_array($type, $allowTypes)) {
            return false;
        }

        $typecode = 'friendcoupon_' . $type;

        $commonModel = m('common');
        $set = $commonModel->getSysset('notice');

        $key = static::WXAPP_PREFIX . $type;// 索引
        $switch_key = $key . static::SWITCH_SUFFIX; // 开关的索引

        
        $data = array(
            'messageId' => $set[$key] ?: 'default', // 消息id是空的话,发送默认客服消息
            /**
             * isOpen这个地方有个问题
             * 之前的模板消息都是走的公众号,那么公众号是这样的
             * pay_template // 支付模板
             * pay_template_close_advanced // 是否关闭状态
             * 小程序的也存到了 m('common')->getSysset('notice')变量下面,声明为
             * wxapp_complete_switch  小程序是否开启模板消息,那么switch = 0 为开启, 1为不开启
             * 所以下面要取反
             */
            'isOpen'    => !$set[$switch_key]
        );

        // 如果信息模板没有开启,直接返回false
        if (!$data['isOpen']) {
            return false;
        }

        if ($data['messageId'] == 'default') {
            // 获取默认模板消息
            return pdo_fetch("select * from " . tablename('ewei_shop_member_wxapp_message_template_default') . " where uniacid = :uniacid and typecode = :typecode", array(
                ':uniacid'  => $_W['uniacid'],
                ':typecode' => $typecode
            ));
        }

        // 用户自定义的模板消息
        return pdo_fetch("select * from " . tablename('ewei_shop_wxapp_tmessage') . " where id = :id", array('id' => $data['messageId']));
    }

    /**
     * 获取公众号或者小程序token
     * @param bool $wxapp
     * @return mixed
     */
    public function getToken($wxapp = false)
    {
        if ($wxapp) {
            return p('app')->getAccessToken();
        }
        return m('common')->getAccount()->fetch_token();
    }


}





